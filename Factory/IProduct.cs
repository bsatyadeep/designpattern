﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Factory
{
    public interface IProduct
    {
        decimal GetPrice();
        string GetImageUrl();
        string GetDescription();
    }
}