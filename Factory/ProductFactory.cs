﻿namespace Factory
{
    public class ProductFactory
    {
        public IProduct MakeProductFromId(string productId)
        {
            if (IsKeyBoard(productId))
                return new Keyboard();
            return new Mouse();
        }

        bool IsKeyBoard(string productId)
        {
            return productId.Substring(0, 1).ToLower().Trim() == "k";
        }
    }
}
