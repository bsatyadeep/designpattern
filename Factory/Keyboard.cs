﻿namespace Factory
{
    public class Keyboard:IProduct
    {
        public decimal GetPrice()
        {
            return 11;
        }

        public string GetImageUrl()
        {
            return "";
        }

        public string GetDescription()
        {
            return "Simple Keyboard";
        }
    }
}