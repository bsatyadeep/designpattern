﻿namespace Factory
{
    public class Mouse:IProduct
    {
        public decimal GetPrice()
        {
            return 10;
        }

        public string GetImageUrl()
        {
            return "";
        }

        public string GetDescription()
        {
            return "Simple Mouse";
        }
    }
}