﻿using System.Collections.Generic;

namespace Factory
{
    public class ShoppingCart
    {
        private readonly ProductFactory _factory;

        public ShoppingCart()
        {
            _factory = new ProductFactory();
        }
        public List<IProduct> Products { get; set; }

        public void AddProduct(string productId)
        {
            Products.Add(_factory.MakeProductFromId(productId));
        }        
    }
}
