﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GatewayandProxy
{
    public class InMemoryCart:ICartGateway
    {
        public Dictionary<string,string> ListOfCarts { get; set; }

        public InMemoryCart()
        {
            ListOfCarts = new Dictionary<string, string>();
        }
        public void Persist(ShoppingCart shoppingCart)
        {
            ListOfCarts.Add(Guid.NewGuid().ToString(),SerializerHelper.Serialize(shoppingCart));
        }

        public ShoppingCart Retrieve(string id)
        {
            return SerializerHelper.DeSerialize(ListOfCarts[id]);
        }

        public string GetIdOfRecordedCart()
        {
            throw new System.NotImplementedException();
        }
    }
}
