﻿using System;
using System.IO;

namespace GatewayandProxy
{
    public class FileCart:ICartGateway
    {
        private string _fileId;

        public FileCart()
        {
            _fileId = Guid.NewGuid().ToString();
        }
        public void Persist(ShoppingCart shoppingCart)
        {
            File.WriteAllText(@"",SerializerHelper.Serialize(shoppingCart));
        }
        public ShoppingCart Retrieve(string id)
        {
            var shoppingCart = SerializerHelper.DeSerialize(id);
            return shoppingCart;
        }

        public string GetIdOfRecordedCart()
        {
            return _fileId;
        }
    }
}
