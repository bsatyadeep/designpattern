﻿using System.Xml;
using System.Xml.Serialization;

namespace GatewayandProxy
{
    public class SerializerHelper
    {
        public static string Serialize(ShoppingCart shoppingCart)
        {
            XmlSerializer ser = new XmlSerializer(shoppingCart.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, shoppingCart);     // Here Classes are converted to XML String. 
                                            // This can be viewed in SB or writer.
                                            // Above XML in SB can be loaded in XmlDocument object
            var xmlString = sb.ToString();
            return xmlString;
        }

        public static ShoppingCart DeSerialize(string xmlString)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlString);
            if (doc.DocumentElement != null)
            {
                XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
                XmlSerializer ser = new XmlSerializer(typeof(ShoppingCart));
                object obj = ser.Deserialize(reader);
                // Then you just need to cast obj into whatever type it is, e.g.:
                ShoppingCart shoppingCart = (ShoppingCart)obj;
                return shoppingCart;
            }
            return null;
        }
    }
}
