﻿namespace GatewayandProxy
{
    public interface ICartGateway
    {
        void Persist(ShoppingCart shoppingCart);

        ShoppingCart Retrieve(string id);

        string GetIdOfRecordedCart();
     }
}