﻿using System.Collections.Generic;

namespace GatewayandProxy
{
    public class ShoppingHistory
    {
        private readonly ICartGateway _gateway;
        public List<ShoppingCart> ShoppingCarts { get; set; }

        public ShoppingHistory(ICartGateway gateway,List<ShoppingCart> shoppingCarts)
        {
            _gateway = gateway;
            ShoppingCarts = shoppingCarts;
        }

        public void ListAllCarts()
        {
            foreach (var shoppingCart in ShoppingCarts)
            {
                _gateway.Retrieve(shoppingCart.ShoppingCartId);
            }
        }
    }
}
