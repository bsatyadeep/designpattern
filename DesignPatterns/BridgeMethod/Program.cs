﻿using System;

namespace BridgeMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            IMessgeSender text = new TextSender();
            IMessgeSender web = new WebServiceSender();

            Message message = new SystemMessage
            {
                Subject = "A Message",
                Body = "Hi there, Please accept this message."
            };
            message.MessgeSender = text;
            message.Send();

            message.MessgeSender = web;
            message.Send();

        }
    }
    //Abstract Class
    public abstract class Message
    {
        public IMessgeSender MessgeSender { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public abstract void Send();
    }
    //RefinedAbstraction Class
    public class SystemMessage : Message
    {
        public override void Send()
        {
            MessgeSender.SendMessage(Subject, Body);
        }
    }
   //the bridge/implementaor interface
    public interface IMessgeSender
    {
        void SendMessage(string subject, string body);
    }
    //concreteimplementor class
    public class TextSender : IMessgeSender
    {
        public void SendMessage(string subject, string body)
        {
            string messageType = "Text";
            Console.WriteLine($"{messageType}");
            Console.WriteLine("--------------");
            Console.WriteLine($"Subject: {subject} from {messageType}\n{body}");
        }
    }
    //The ConcreteImplementaor
    public class WebServiceSender : IMessgeSender
    {
        public void SendMessage(string subject, string body)
        {
            string messageType = "Web Service";
            Console.WriteLine($"{messageType}");
            Console.WriteLine("--------------");
            Console.WriteLine($"Subject: {subject} from {messageType}\n{body}");
        }
    }
}
