﻿using System;
using System.Collections.Generic;

namespace AdapterMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Emploee list from third party organization system.");
            Console.WriteLine("-------------------------------------------------");
            //client will use ITarget interface to call functinality of
            // Adapatee class i.e. ThirstPartyEmployee
            ITarget target = new EmployeeAdapter();
            var employees = target.GetEmployees();
            foreach (var employee in employees)
            {
                Console.WriteLine(employee);
            }
        }
    }
    // Ataptee Class
    public class ThirdPartyEmployee
    {
        public List<string> GetEmployeeList()
        {
            List<string> employeeList = new List<string>
            {
                "Peter",
                "Paul",
                "Puru",
                "prachi"
            };
            return employeeList;
        }
    }
    // ITarget interface
    public interface ITarget
    {
        List<string> GetEmployees();
    }
    // Adapter Class
    public class EmployeeAdapter:ThirdPartyEmployee,ITarget
    {
        public List<string> GetEmployees()
        {
            return GetEmployeeList();
        }
    }
}
