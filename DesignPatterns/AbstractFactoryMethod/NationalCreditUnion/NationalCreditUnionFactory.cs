﻿using System;
using AbstractFactoryMethod.Interface;

namespace AbstractFactoryMethod.NationalCreditUnion
{
    // Concrete Factory 2
    public class NationalCreditUnionFactory: ICreditUnionFactory
    {
        public override ISavingsAccount CreateSavingsAccount()
        {
            return new NationalSavingsAccount();
        }

        public override ILoanAccount CreateLoanAccount()
        {
            return new NationaLoanAccount();
        }
    }

    // Concrete Product A2
    public class NationalSavingsAccount:ISavingsAccount
    {
        public NationalSavingsAccount()
        {
            Console.WriteLine("Returned National Savings Account");
        }
    }

    // Concrete Product B2
    public class NationaLoanAccount : ILoanAccount
    {
        public NationaLoanAccount()
        {
            Console.WriteLine("Returned National Loan Account");
        }
    }
}