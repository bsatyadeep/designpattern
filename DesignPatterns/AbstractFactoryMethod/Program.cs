﻿using System;
using System.Collections.Generic;
using AbstractFactoryMethod.Interface;
using AbstractFactoryMethod.Providers;

namespace AbstractFactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> accountnos = new List<string>
            {
                "CITY-456",
                "NATIONAL-987",
                "CHASE-222"
            };
            foreach (var accountno in accountnos)
            {
                ICreditUnionFactory abstractFactory = CreditUnionFactoryProvider.GetCreditUnionFactory(accountno);
                if (abstractFactory == null)
                {
                    Console.WriteLine("Sorry this credit union w/ account no {0} is invalid",accountno);
                }
                else
                {
                    ILoanAccount loanAccount = abstractFactory.CreateLoanAccount();
                    ISavingsAccount savingsAccount = abstractFactory.CreateSavingsAccount();
                }
            }
        }
    }
}
