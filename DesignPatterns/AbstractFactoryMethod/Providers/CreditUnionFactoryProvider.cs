﻿using AbstractFactoryMethod.CityCreditUnion;
using AbstractFactoryMethod.Interface;
using AbstractFactoryMethod.NationalCreditUnion;

namespace AbstractFactoryMethod.Providers
{
    public class CreditUnionFactoryProvider
    {
        public static ICreditUnionFactory GetCreditUnionFactory(string accountno)
        {
            if (accountno.Contains("CITY"))
            {
                return new CityCreditUnionFactory();
            }
            if (accountno.Contains("NATIONAL"))
            {
                return new NationalCreditUnionFactory();
            }
            return null;
        }
    }
}
