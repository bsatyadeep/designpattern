﻿using System;
using AbstractFactoryMethod.Interface;

namespace AbstractFactoryMethod.CityCreditUnion
{
    // Concrete Factory 1
    public class CityCreditUnionFactory : ICreditUnionFactory
    {
        public override ISavingsAccount CreateSavingsAccount()
        {
            return new CitySavingAccount();
        }

        public override ILoanAccount CreateLoanAccount()
        {
            return new CityLoanAccount();
        }
    }

    // Concrete Product A1
    public class CitySavingAccount : ISavingsAccount
    {
        public CitySavingAccount()
        {
            Console.WriteLine("Returned City Savings Account");
        }
    }
    // Concerete Product B1
    public class CityLoanAccount : ILoanAccount
    {
        public CityLoanAccount()
        {
            Console.WriteLine("Returned City Loan Account");
        }
    }
}