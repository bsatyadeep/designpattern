﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Repository.Dal;
using Repository.Models;

namespace Repository.Controllers
{
    public class StudentsController : Controller
    {
        //private RepositoryContext db = new RepositoryContext();

        private IStudentRepository _studentRepository;

        public StudentsController()
        {
            _studentRepository = new StudentRepository(new RepositoryContext());
        }

        // GET: Students
        public async Task<ActionResult> Index()
        {
            //return View(await db.Students.ToListAsync());
            return View(await _studentRepository.GetStudents());
        }

        // GET: Students/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Student student = await db.Students.FindAsync(id);
            Student student = await _studentRepository.GetStudentById(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StudentId,Name,Year")] Student student)
        {
            if (ModelState.IsValid)
            {
                //db.Students.Add(student);
                //await db.SaveChangesAsync();
                _studentRepository.InsertStudent(student);
                await _studentRepository.Save();
                return RedirectToAction("Index");
            }

            return View(student);
        }

        // GET: Students/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Student student = await db.Students.FindAsync(id);
            Student student = await _studentRepository.GetStudentById(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StudentId,Name,Year")] Student student)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(student).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                _studentRepository.UpdateStudent(student);
                await _studentRepository.Save();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Student student = await db.Students.FindAsync(id);
            Student student = await _studentRepository.GetStudentById(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            //Student student = await db.Students.FindAsync(id);
            //db.Students.Remove(student);
            //await db.SaveChangesAsync();
            _studentRepository.DeleteStudent(id);
            await _studentRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
                _studentRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
