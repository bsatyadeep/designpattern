﻿using System;
using Repository.Models;

namespace Repository.Dal
{
    public class UnitOfWork:IUnitOfWork
    {
        // Part 1
        private RepositoryContext _context = new RepositoryContext();
        private GenericRepository<Faculty> facultyRepository;
        private GenericRepository<Student> studentRepository;

        public GenericRepository<Faculty> FacultyRepository =>
            facultyRepository ?? new GenericRepository<Faculty>(_context);

        public GenericRepository<Student> StudentRepository =>
            studentRepository ?? new GenericRepository<Student>(_context);

       
        // Part 2
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            // Prevent the GC from calling Object.Finalize on an 
            // object that does not require it
            GC.SuppressFinalize(this);
        }
    }
}