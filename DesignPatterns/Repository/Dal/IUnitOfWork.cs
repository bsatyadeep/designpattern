﻿using System;
using Repository.Models;

namespace Repository.Dal
{
    public interface IUnitOfWork:IDisposable
    {
        void Save();
        GenericRepository<Faculty> FacultyRepository { get; }
        GenericRepository<Student> StudentRepository { get; }
    }
}