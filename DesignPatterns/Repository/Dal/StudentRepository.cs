﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Repository.Models;

namespace Repository.Dal
{
    public class StudentRepository:IStudentRepository
    {
        // Part 1
        private readonly RepositoryContext _context;

        // Constrcutor
        public StudentRepository(RepositoryContext context)
        {
            _context = context;
        }


        // Part 3 - Clean up
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            // Prevent the GC from calling Object.Finalize on an 
            // object that does not require it
            GC.SuppressFinalize(this);
        }

        public Task<List<Student>> GetStudents()
        {
            return _context.Students.ToListAsync();
        }

        public Task<Student> GetStudentById(int? studentId)
        {
            return _context.Students.FindAsync(studentId);
        }

        public void InsertStudent(Student student)
        {
            _context.Students.Add(student);
        }

        public void DeleteStudent(int studentId)
        {
            var student = _context.Students.Find(studentId);
            if (student !=null)
            {
                _context.Students.Remove(student);
            }
        }

        public void UpdateStudent(Student student)
        {
            _context.Entry(student).State = EntityState.Modified;
        }

        public Task<int> Save()
        {
            return _context.SaveChangesAsync();
        }
    }
}