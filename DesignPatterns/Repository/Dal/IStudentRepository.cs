﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Repository.Models;

namespace Repository.Dal
{
    public interface IStudentRepository:IDisposable
    {
        Task<List<Student>> GetStudents();
        Task<Student> GetStudentById(int? studentId);
        void InsertStudent(Student student);
        void DeleteStudent(int studentId);
        void UpdateStudent(Student student);
        Task<int> Save();
    }
}