﻿namespace Repository.Models
{
    public class Faculty
    {
        public int FacultyId { get; set; }
        public string Name { get; set; }
    }
}