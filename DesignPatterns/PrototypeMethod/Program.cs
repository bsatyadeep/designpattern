﻿using System;
using System.Collections.Generic;

namespace PrototypeMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            ColorController colorController = new ColorController(new Dictionary<string, ColorPrototype>());
            //Initialize With standard colors
            colorController["Yellow"] = new Color(255,255,0);
            colorController["Ornage"] = new Color(255,128,0);
            colorController["Purple"] = new Color(128,0,255);

            //User adds Persionalized Colors
            colorController["Sunny"] = new Color(255, 54, 0);
            colorController["Testy"] = new Color(255, 153, 51);
            colorController["Rainy"] = new Color(255, 0, 255);

            //User Clones
            Color c1 = colorController["Yellow"].Clone() as Color;
            Color c2 = colorController["Testy"].Clone() as Color;
            Color c3 = colorController["Rainy"].Clone() as Color;

        }
    }
    //Prototype abstract class
    public abstract class ColorPrototype
    {
        public abstract ColorPrototype Clone();
    }
    //ConcretePrototype Class
    public class Color : ColorPrototype
    {
        private int _yellow;
        private int _orange;
        private int _purple;

        public Color(int yellow, int orange, int purple)
        {
            _yellow = yellow;
            _orange = orange;
            _purple = purple;
        }

        public override ColorPrototype Clone()
        {
            Console.WriteLine("RGB Color is cloned to: {0,3},{1,3},{2,3}",_yellow,_orange,_purple);
            return MemberwiseClone() as ColorPrototype;
        }
    }
    //Prototype Manager
    public class ColorController
    {
        private Dictionary<string,ColorPrototype> _Colors;

        public ColorController(Dictionary<string, ColorPrototype> colors)
        {
            _Colors = colors;
        }

        //Indexer
        public ColorPrototype this[string key]
        {
            get => _Colors[key];
            set => _Colors.Add(key,value);
        }
    }
}