﻿using System;

namespace FactoryMethodExample1
{
    public abstract class IProduct
    {
        public Guid ProductId { get; set; }
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }
    }
}