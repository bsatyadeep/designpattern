﻿using System.Collections.Generic;

namespace FactoryMethodExample1
{
    public class ShoppingCart:IOrderItems
    {
        private List<IProduct> _products = null;
        public List<IProduct> Products => _products ?? (_products = new List<IProduct>());

        public IProduct GetProductType(string productName)
        {
            IProduct product = null;
            if (!string.IsNullOrWhiteSpace(productName))
            {
                var substring = productName.Substring(0,1).ToUpper();
                switch (substring)
                {
                    case "K":
                        product = new Keyboard();
                        break;
                    case "M":
                        product = new Mouse();
                        break;
                }
            }
            return product;
        }

        public void AddProduct(IProduct product)
        {
            Products.Add(product);
        }
        
    }
}