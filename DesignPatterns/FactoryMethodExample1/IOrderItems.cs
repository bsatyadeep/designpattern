﻿using System.Collections.Generic;

namespace FactoryMethodExample1
{
    public interface IOrderItems
    {
        IProduct GetProductType(string productName);
        void AddProduct(IProduct product);
    }
}