﻿using System;

namespace FactoryMethodExample1
{
    public class Mouse: IProduct
    {
        public Mouse()
        {
            ProductId = Guid.NewGuid();
            Name = "Lenovo Wireless Mouse";
            Price = 100;
        }
    }
}