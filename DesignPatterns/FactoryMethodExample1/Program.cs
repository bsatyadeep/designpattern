﻿using System;

namespace FactoryMethodExample1
{
    class Program
    {
        static void Main(string[] args)
        {
            var cart = new ShoppingCart();
            var keyboard = cart.GetProductType("KLenovo");
            cart.AddProduct(keyboard);

            var mouse = cart.GetProductType("M");
            cart.AddProduct(mouse);
            foreach (var cartProduct in cart.Products)
            {
                Console.WriteLine("Product: {0} pice is: {1:C0}",cartProduct.Name,cartProduct.Price);
            }
        }
    }
}
