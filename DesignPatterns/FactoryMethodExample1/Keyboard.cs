﻿using System;

namespace FactoryMethodExample1
{
    public class Keyboard:IProduct
    {
        public Keyboard()
        {
            ProductId = Guid.NewGuid();
            Price = 500;
            Name = "Lenovo Keyboard";
        }        
    }
}