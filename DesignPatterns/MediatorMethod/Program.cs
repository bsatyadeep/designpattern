﻿using System;
using System.Collections.Generic;

namespace MediatorMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create Chat Room
            ChatRoom chatRoom = new ChatRoom();
            Participant eddie = new Actor("Eddie");
            Participant jeniffer = new Actor("Jeniffer");
            Participant brush = new Actor("Brush");
            Participant tom = new Actor("Tom");
            Participant tonny = new NonActor("Tonny");

            // Create Participants and register them
            chatRoom.Register(eddie);
            chatRoom.Register(jeniffer);
            chatRoom.Register(brush);
            chatRoom.Register(tom);
            chatRoom.Register(tonny);

            // Chatting Participant
            tonny.Send("Tom","Hey Tom! I got a mission for you.");
            jeniffer.Send("Brush","Teach me to act and I'll" +
                                  " teach to dance.");
            brush.Send("Eddie","How come you don't do standup anymore?");
            jeniffer.Send("Tom","Do you like Match?");

            tom.Send("Tonny","Teach me to Sing.");


        }
    }

    // Medicator Abstract class
    public abstract class AbstractChatRoom
    {
        public abstract void Register(Participant participant);
        public abstract void Send(string from, string to, string message);
    }
    

    //ConcreteMediator Class
    public class ChatRoom:AbstractChatRoom
    {
        private Dictionary<string, Participant> _participants;

        public ChatRoom()
        {
            _participants = new Dictionary<string, Participant>();
        }

        public override void Register(Participant participant)
        {
            if (!_participants.ContainsKey(participant.Name))
            {
                _participants.Add(participant.Name,participant);
            }

            participant.Chatroom = this;
        }

        public override void Send(string @from, string to, string message)
        {
            Participant participant = _participants[to];
            participant?.Receive(@from, message);
        }
    }

    //AbstractColleague class
    public class Participant
    {
        public string Name { get; }
        public ChatRoom Chatroom { get; set; }

        public Participant(string name)
        {
            Name = name;
        }

        public void Send(string to, string message)
        {
            Chatroom.Send(Name, to, message);
        }
        public virtual void Receive(string @from, string message)
        {
            Console.WriteLine($"{from} to {Name}: {message}");
        }
    }
    // ConcreteColleague Class
    public class Actor : Participant
    {
        public Actor(string name) : base(name)
        {
        }
        public override void Receive(string @from, string message)
        {
            Console.WriteLine("To a Actor:");
            base.Receive(@from, message);
        }
    }
    //ConcreteColleague Class
    public class NonActor : Participant
    {
        public NonActor(string name) : base(name)
        {
        }

        public override void Receive(string @from, string message)
        {
            Console.WriteLine("To a non-Actor:");
            base.Receive(@from, message);
        }
    }
}
