﻿using System;

namespace FacadeMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            //Facade
            CollegeLoan collegeLoan = new CollegeLoan();

            //Evaluate loan
            Student student = new Student {Name = "Hunter Sky"};
            var isEligible = collegeLoan.IsEligible(student,75000);
            Console.WriteLine($"{student.Name} has been {(isEligible?"Approved":"Rejected")}");
        }
    }

    public class CollegeLoan
    {
        private Bank _bank = new Bank();
        private Loan _loan = new Loan();
        private Credit _credit = new Credit();

        public bool IsEligible(Student student, int amount)
        {
            bool eligible = true;
            Console.WriteLine($"{student.Name} applies for {amount:C} loan\n");

            //Verify credit worthness of applicant
            if (!_bank.HasSufficientSavings(student,amount))
            {
                eligible = false;
            }else if (!_loan.HasNoBadLoans(student))
            {
                eligible = false;
            }
            else if (!_credit.HasGoodCredit(student))
            {
                eligible = false;
            }
            return eligible;
        }
    }

    public class Student
    {
        public string Name { get; set; }
    }
    //Subsystem ClassB
    public class Credit
    {
        public bool HasGoodCredit(Student student)
        {
            Console.WriteLine($"Verify credit for {student.Name}");
            return true;
        }
    }
    //Subsystem ClassC
    public class Loan
    {
        public bool HasNoBadLoans(Student student)
        {
            Console.WriteLine($"Verify loan for {student.Name}");
            return true;
        }
    }
    //SubSystem ClassA
    public class Bank
    {
        public bool HasSufficientSavings(Student student, int amount)
        {
            Console.WriteLine($"Verify bank for {student.Name}");
            return true;
        }
    }
}
