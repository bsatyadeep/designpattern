﻿using System;
using System.Collections.Generic;

namespace BuilderMethod
{
    class Program
    {
        // Client
        static void Main(string[] args)
        {
            var superBuilder = new SuperCarBuilder();
            var notSoSuperBuilder = new NotSoSuperCarBuilder();

            var carFactory = new CarFactory();
            var carBuilders = new List<CarBuilder>
            {
                superBuilder,
                notSoSuperBuilder
            };

            foreach (var carBuilder in carBuilders)
            {
                var c = carFactory.Build(carBuilder);
                Console.WriteLine("The Car requested by " +
                                  $"{carBuilder.GetType().Name}: " +
                                  "\n-------------------------------------------" +
                                  $"\nHorse Power: {c.HoursePower}" +
                                  $"\nTop Speed: {c.TopSpeed} mph" +
                                  $"\nImpressive Feature: {c.MostImpressiveFeature}\n");
            }
        }
    }
    // Product Class
    public class Car
    {
        public int TopSpeed { get; set; }
        public int HoursePower { get; set; }
        public string MostImpressiveFeature { get; set; }
    }
    // Builder Abstract Class
    public abstract class CarBuilder
    {
        protected readonly Car _car = new Car();
        public abstract void SetTopSpeed();
        public abstract void SetHoursePower();
        public abstract void SetImpressiveFeature();
        public virtual Car GetCar()
        {
            return _car;
        }
    }
    // Director Class
    public class CarFactory
    {
        public Car Build(CarBuilder builder)
        {
            builder.SetTopSpeed();
            builder.SetHoursePower();
            builder.SetImpressiveFeature();
            var car = builder.GetCar();
            return car;
        }
    }
    // ConcreteBuilder1 class
    public class NotSoSuperCarBuilder: CarBuilder
    {
        public override void SetTopSpeed()
        {
            _car.HoursePower = 120;
        }

        public override void SetHoursePower()
        {
            _car.TopSpeed = 70;
        }

        public override void SetImpressiveFeature()
        {
            _car.MostImpressiveFeature = "Has Air Conditioning";
        }
    }
    // ConcreteBuilder2 class
    public class SuperCarBuilder: CarBuilder
    {
        public override void SetTopSpeed()
        {
            _car.TopSpeed = 600;
        }
        public override void SetHoursePower()
        {
            _car.HoursePower = 1640;
        }
        public override void SetImpressiveFeature()
        {
            _car.MostImpressiveFeature = "Can Fly";
        }
    }
}
