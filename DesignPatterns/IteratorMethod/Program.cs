﻿using System;
using IteratorMethod.Aggregate;
using IteratorMethod.Iterator;

namespace IteratorMethod
{
    class Program
    {
        static void Main()
        {
            INewspaper nyt = new NyPaper();
            INewspaper lat = new LaPaper();

            IIterator nypIterator = nyt.CreateIterator();
            IIterator latIterator = lat.CreateIterator();

            Console.WriteLine("---------- NY Paper -----------");
            PrintReporters(nypIterator);

            Console.WriteLine("---------- LA Paper -----------");
            PrintReporters(latIterator);

            Console.ReadLine();
        }

        static void PrintReporters(IIterator iterator)
        {
            iterator.First();
            while (!iterator.IsDone())
            {
                Console.WriteLine(iterator.Next());
            }
        }
    }
}
