﻿using System.Collections.Generic;
using System.Linq;

namespace IteratorMethod.Iterator
{
    public class NyPaperIterator : IIterator
    {
        private List<string> _reporters;
        private int _current;
        public NyPaperIterator(List<string> reporters)
        {
            _reporters = reporters;
            _current = 0;
        }

        public IIterator CreateIterator()
        {
            throw new System.NotImplementedException();
        }

        public void First()
        {
            _current = 0;
        }

        public string Next()
        {
            return _reporters.ElementAt(_current++);
        }

        public bool IsDone()
        {
            return _current >= _reporters.Count;
        }

        public string CurrentItem()
        {
            return _reporters.ElementAt(_current);
        }
    }
}