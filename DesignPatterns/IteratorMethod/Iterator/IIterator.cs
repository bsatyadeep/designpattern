﻿namespace IteratorMethod.Iterator
{
    // Iterator
    public interface IIterator
    {
        IIterator CreateIterator();
        void First();   // Sets current element to the first element
        string Next();  // Advance current to Next
        bool IsDone();  // Check if end of collection
        string CurrentItem();   // returns the current element
    }
}