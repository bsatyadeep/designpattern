﻿namespace IteratorMethod.Iterator
{
    public class LaPaperIterator : IIterator
    {
        private string[] _reporters;
        private int _current;

        public LaPaperIterator(string[] reporters)
        {
            _reporters = reporters;
            _current = 0;
        }

        public IIterator CreateIterator()
        {
            throw new System.NotImplementedException();
        }

        public void First()
        {
            _current = 0;
        }

        public string Next()
        {
            return _reporters[_current++];
        }

        public bool IsDone()
        {
            return _current >= _reporters.Length;
        }

        public string CurrentItem()
        {
            return _reporters[_current];
        }
    }
}