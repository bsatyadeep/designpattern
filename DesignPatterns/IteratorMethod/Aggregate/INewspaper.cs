﻿using IteratorMethod.Iterator;

namespace IteratorMethod.Aggregate
{
    //Aggregate
    public interface INewspaper
    {
        IIterator CreateIterator();
    }
}