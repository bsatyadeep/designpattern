﻿using System.Collections.Generic;
using IteratorMethod.Iterator;

namespace IteratorMethod.Aggregate
{
    public class NyPaper:INewspaper
    {
        // ConceteAggregate
        private List<string> _reporters;

        public NyPaper()
        {
            _reporters = new List<string>
            {
                "John Mesh - NY",
                "Susanna Lee - NY",
                "Paul Randy - NY",
                "Kim Fields - NY",
                "Sky Taylor - NY",
            };
        }
        public IIterator CreateIterator()
        {
            return new NyPaperIterator(_reporters);
        }
    }
}