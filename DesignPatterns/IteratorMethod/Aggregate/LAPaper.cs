﻿using IteratorMethod.Iterator;

namespace IteratorMethod.Aggregate
{
    // Concrete Aggregate
    public class LaPaper:INewspaper
    {
        private string[] _reporters;

        public LaPaper()
        {
            _reporters = new[]
            {
                "Ronald Smith - LA",
                "Dany Glover - LA",
                "Jery Stright - LA",
                "Ronald Lime - LA",
                "Yolanda Adams - LA"
            };
        }
        public IIterator CreateIterator()
        {
            return new LaPaperIterator(_reporters);
        }
    }
}