﻿using Decorator.Component;

namespace Decorator.ConcreteComponent
{
    // Concerete Component
    public class FullSizeCar:Car
    {
        public FullSizeCar()
        {
            Description = "Full Size Card";
        }
        public override string GetDescription() => Description;
        public override double GetCardPrice() => 30000.00;
    }
}