﻿using Decorator.Component;
using Decorator.Decorator;

namespace Decorator.ConcreteDecorator
{
    public class Sunroof:CarDecorator
    {
        public Sunroof(Car car) : base(car)
        {
            Description = "Car with Sunroof Protection";
        }
        public override string GetDescription() => $"{_car.GetDescription()}, {Description}";
        public override double GetCardPrice() => _car.GetCardPrice() + 2500;
    }
}