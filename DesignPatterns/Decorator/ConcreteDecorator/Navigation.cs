﻿using Decorator.Component;
using Decorator.Decorator;

namespace Decorator.ConcreteDecorator
{
    public class Navigation:CarDecorator
    {
        public Navigation(Car car) : base(car)
        {
            Description = "Car with navigation";
        }
        public override string GetDescription() => $"{_car.GetDescription()}, {Description}";
        public override double GetCardPrice() => _car.GetCardPrice() + 2500;
    }
}