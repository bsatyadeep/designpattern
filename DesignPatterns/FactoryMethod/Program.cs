﻿//Providing interfac for creating families of related or depedent objects withour specifying there concreate classes

using System;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new SavingsFactory() as ICreditUnionFactory;
            var cityAccount = factory.GetSavingsAccount("CITY-321");
            var nationalAccount = factory.GetSavingsAccount("NATIONAL-987");

            Console.WriteLine($"My citi balance is ${cityAccount.Balance}"+
                              $" and national balance is ${nationalAccount.Balance}");
        }
    }
}

// Product
public abstract class ISavingsAccount
{
    public decimal Balance { get; set; }
}

// Concrete Product
public class CitySavingsAccount: ISavingsAccount
{
    public CitySavingsAccount()
    {
        Balance = 5000;
    }
}
// Concrete Product
public class NationalSavingsAccount:ISavingsAccount
{
    public NationalSavingsAccount()
    {
        Balance = 2000;
    }
}

// Creator
public interface ICreditUnionFactory
{
    ISavingsAccount GetSavingsAccount(string accountNo);
}
// Concrete Creator
public class SavingsFactory:ICreditUnionFactory
{
    public ISavingsAccount GetSavingsAccount(string accountNo)
    {
        if (accountNo.Contains("CITY"))
        {
            return new CitySavingsAccount();
        }
        if (accountNo.Contains("NATIONAL"))
        {
            return new NationalSavingsAccount();
        }
        return null;
    }
}

#region Interview Test

//interface IA
//{

//}

//interface IB
//{

//}

//public class MyBaseClass
//{

//}
//partial class MyDeriveClass:MyBaseClass
//{

//}
// This is possible but generates compiler errors 
// if Interface comes before class in device class implementation
//class MyDeriveClass : MyBaseClass, IA,IB
//{

//}

#endregion