﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SingletonMethod;

namespace Singleton.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IsPolicyIsSingleton()
        {
            Assert.AreSame(Policy.Instance,Policy.Instance);
        }
    }
}
