﻿using System;
using ObserverMethod.Observer;
using ObserverMethod.Subject;

namespace ObserverMethod.ConcreteObserver
{
    // Concrete Observer
    public class Fan:IFan
    {
        public void Update(ICelebrity celebrity)
        {
            Console.WriteLine($"Fan Notified. Tweet of {celebrity.FullName}: {celebrity.Tweet}");
        }
    }
}