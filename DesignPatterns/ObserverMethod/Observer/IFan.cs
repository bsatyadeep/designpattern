﻿using ObserverMethod.Subject;

namespace ObserverMethod.Observer
{
    // Observer
    public interface IFan
    {
        void Update(ICelebrity celebrity);
    }
}