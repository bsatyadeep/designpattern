﻿using System;
using ObserverMethod.ConcreteObserver;
using ObserverMethod.ConcreteSubject;

namespace ObserverMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            var gClooney = new GClooney("I Love my Wife");
            var tSwift = new TSwift("1981 is my favorite number");

            var firstFan = new Fan();
            var secondFan = new Fan();

            gClooney.AddFollower(firstFan);
            tSwift.AddFollower(secondFan);

            gClooney.Tweet = "My wife didn't force me to tweet";
            tSwift.Tweet = "I love my new misuc";

            Console.ReadLine();
        }
    }
}
