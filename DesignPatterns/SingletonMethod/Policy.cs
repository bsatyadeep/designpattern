﻿namespace SingletonMethod
{
    public class Policy
    {
        private int Id { get; set; } = 123;
        private string Insured { get; set; } = "John Roy";
        public string GetInsuredName() => Insured;

        public static Policy Instance
        {
            get
            {
                //lock (_lock)
                //{
                //    return _instancePolicy ?? (_instancePolicy = new Policy());
                //}

                return InstancePolicy;
            }
        }
        //private static Policy _instancePolicy;
        //private static readonly object _lock =new object();
        private static readonly Policy InstancePolicy = new Policy();
    }
}