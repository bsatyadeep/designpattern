﻿using System;

namespace SingletonMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            //var policy = new Policy();
            //var insuredName = policy.GetInsuredName();
            var insuredName = Policy.Instance.GetInsuredName();
            Console.WriteLine(insuredName);
        }
    }
}
