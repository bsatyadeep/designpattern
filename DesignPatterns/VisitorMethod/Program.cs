﻿using System;
using System.Collections.Generic;

namespace VisitorMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            //Setup employee colleague
            Employees e = new Employees();
            e.Attach(new Clerk());
            e.Attach(new Director());
            e.Attach(new President());

            //Employees are Visited

            e.Accept(new IncomeVisitor());
            e.Accept(new VacationVisitor());


        }
    }
    // Visitor Interface
    public interface IVisitor
    {
        void Visit(Element element);
    }
    //ConcreteVisitor Class
    public class IncomeVisitor : IVisitor
    {
        public void Visit(Element element)
        {
            Employee employee = element as Employee;

            // Provide 10% pay rise
            Console.WriteLine($"{employee.GetType().Name} {employee.Name}'s income: {employee.Income:C}");
        }
    }
    //ConcreteCisitor Class
    public class VacationVisitor : IVisitor
    {
        public void Visit(Element element)
        {
            Employee employee = element as Employee;

            //Provide 3 extra vaction days
            Console.WriteLine($"{employee.GetType().Name} {employee.Name}'s new Vacation days: {employee.VacationDays}");
        }
    }
    
    //Eement Abstract Class
    public abstract class Element
    {
        public abstract void Accept(IVisitor visitor);
    }
    // The Concrete Element
    public class Employee:Element
    {
        public string Name { get; set; }
        public decimal Income { get; set; }
        public int VacationDays { get; set; }
        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
    // The ObjectStructure
    public class Employees
    {
        private List<Employee> _employees = new List<Employee>();

        public void Attach(Employee employee)
        {
            _employees.Add(employee);
        }

        public void Detach(Employee employee)
        {
            _employees.Remove(employee);
        }

        public void Accept(IVisitor visitor)
        {
            foreach (var employee in _employees)
            {
                employee.Accept(visitor);
            }

            Console.WriteLine();
        }
    }
    //Three Employee types
    public class Clerk : Employee
    {
        public Clerk()
        {
            Name = "Harry";
            Income = 25000.0M;
            VacationDays = 14;
        }
    }
    //Three Employee types
    public class Director : Employee
    {
        public Director()
        {
            Name = "Edward";
            Income = 35000.0M;
            VacationDays = 16;
        }
    }
    //Three Employee types
    public class President : Employee
    {
        public President()
        {
            Name = "Damond";
            Income = 45000.0M;
            VacationDays = 21;
        }
    }
}
