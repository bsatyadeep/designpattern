﻿using System;

namespace ProxyMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculateProxy proxy = new CalculateProxy(new Math());

            Console.WriteLine("Calculations");
            Console.WriteLine("------------");
            Console.WriteLine($"\n10+5 = {proxy.Add(10,5)}");
            Console.WriteLine($"\n10-5 = {proxy.Subtract(10,5)}");
            Console.WriteLine($"\n10*5 = {proxy.Multiply(10,5)}");
            Console.WriteLine($"\n10/5 = {proxy.Divide(10,5)}");
        }
    }
    //Subject Interface
    public interface IMath
    {
        double Add(double x, double y);
        double Subtract(double x, double y);
        double Multiply(double x, double y);
        double Divide(double x, double y);
    }
    //RealObject class
    public class Math:IMath
    {
        public double Add(double x, double y)
        {
            return x + y;
        }

        public double Subtract(double x, double y)
        {
            return x - y;
        }

        public double Multiply(double x, double y)
        {
            return x * y;
        }

        public double Divide(double x, double y)
        {
            return x / y;
        }
    }
    // The ProxyObject Class
    public class CalculateProxy:IMath
    {
        private Math _math;

        public CalculateProxy(Math math)
        {
            _math = math;
        }

        public double Add(double x, double y)
        {
            return _math.Add(x, y);
        }

        public double Subtract(double x, double y)
        {
            return _math.Subtract(x, y);
        }

        public double Multiply(double x, double y)
        {
            return _math.Multiply(x, y);
        }

        public double Divide(double x, double y)
        {
            return _math.Divide(x, y);
        }
    }

}
