﻿using System;
using System.Collections.Generic;

namespace CommandMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create user & let them compute
            User user = new User(new Calculator(), new List<Command>());

            // User press calculator button
            user.Compute('+',100);
            user.Compute('-',50);
            user.Compute('*',10);
            user.Compute('/',2);

            // Undo 4 Commands
            user.Undo(4);

            // Redo Method
            // lets redo 3 levels
            user.Redo(3);
        }
    }
    // Command abstract class
    public abstract class Command
    {
        public abstract void Execute();
        public abstract void UnExecute();
    }

    // ConcreteCommand Class
    public class CalculatorCommand : Command
    {
        private char _operator;
        private int _operand;
        private Calculator _calculator;

        public CalculatorCommand(char @operator, int operand, Calculator calculator)
        {
            _operator = @operator;
            _operand = operand;
            _calculator = calculator;
        }

        public override void Execute()
        {
            _calculator.Operation(_operator, _operand);
        }

        public override void UnExecute()
        {
            _calculator.Operation(Undo(_operator), _operand);
        }

        // returns opposite operator for a given operator
        private char Undo(char @operator)
        {
            switch (@operator)
            {
                case '+':
                    return '-';
                case '-':
                    return '+';
                case '*':
                    return '/';
                case '/':
                    return '*';
                default:
                    throw new ArgumentException("@Operator");
            }
        }
    }
    // The receiver class
    public class Calculator
    {
        private int _current = 0;
        public void Operation(char @operator, int operand)
        {
            switch (@operator)
            {
                case '+':
                    _current += operand;
                    break;
                case '-':
                    _current -= operand;
                    break;
                case '*':
                    _current *= operand;
                    break;
                case '/':
                    _current /= operand;
                    break;
            }

            Console.WriteLine($"Current value = {_current} (following {@operator} {operand})");
        }
    }
    // The Invoker class
    public class User
    {
        private Calculator _calculator;
        private List<Command> _commands;
        private int _current;

        public User(Calculator calculator, List<Command> commands)
        {
            _calculator = calculator;
            _commands = commands;
            _current = 0;
        }

        public void Redo(int levels)
        {
            Console.WriteLine($"\n--- Redo {levels}");
            //Performs redo operation
            for (int i = 0; i < levels; i++)
            {
                if (_current<_commands.Count+1)
                {
                    Command command = _commands[_current++];
                    command.Execute();
                }
            }
        }

        public void Undo(int levels)
        {
            Console.WriteLine($"\n--- Undo {levels}");
            //Performs redo operation
            for (int i = 0; i < levels; i++)
            {
                if (_current >0)
                {
                    Command command = _commands[--_current];
                    command.UnExecute();
                }
            }
        }

        public void Compute(char @operator, int operand)
        {
            //Create command operation ad excute it
            Command command = new CalculatorCommand(@operator,operand,_calculator);
            command.Execute();

            //Add command to undo list
            _commands.Add(command);
            _current++;
        }
    }
}
