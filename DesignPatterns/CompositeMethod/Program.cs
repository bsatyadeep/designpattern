﻿using System;
using System.Collections.Generic;

namespace CompositeMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee ricky = new Employee {EmployeeId = 1, Name = "Ricky", Rating = 3};
            Employee mick = new Employee {EmployeeId = 2, Name = "Mick", Rating = 4};
            Employee mariyan = new Employee {EmployeeId = 3, Name = "Mariyan", Rating = 5};
            Employee ginger = new Employee {EmployeeId = 4, Name = "Ginger", Rating = 3};
            Employee olive = new Employee {EmployeeId = 5, Name = "Olive", Rating = 4};
            Employee candy = new Employee {EmployeeId = 6, Name = "Candy", Rating = 5};

            Supervisor ronny = new Supervisor {EmployeeId = 7, Name = "Ronny",Rating = 3};
            Supervisor bobby = new Supervisor {EmployeeId = 8, Name = "Bobby",Rating = 3};

            ronny.AddSubordinates(ricky);
            ronny.AddSubordinates(mick);
            ronny.AddSubordinates(mariyan);

            bobby.AddSubordinates(ginger);
            bobby.AddSubordinates(olive);
            bobby.AddSubordinates(candy);

            Console.WriteLine("\n--- Employee can see their performance " +
                              "Summary ---");
            ricky.PerformanceSummary();

            Console.WriteLine("\n--- Supervisor can also see their subordinates " +
                              " performance Summary ---");
            ronny.PerformanceSummary();
            Console.WriteLine("\nSubordinates performance record:");
            foreach (var subordinate in ronny.Subordinates)
            {
                subordinate.PerformanceSummary();
            }
        }
    }
    // IComponent Interface
    public interface IEmployee
    {
        int EmployeeId { get; set; }
        string Name { get; set; }
        int Rating { get; set; }
        void PerformanceSummary();
    }
    // Leaf Class -- will be leaf node in tree structure
    public class Employee:IEmployee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }
        public void PerformanceSummary()
        {
            Console.WriteLine("\nPerformance summary of employee: " +
                              $"{Name} is {Rating} out of 5");
        }
    }
    // Composite class - will be branch node in the tree structure
    public class Supervisor:IEmployee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int Rating { get; set; }

        
        //public List<IEmployee> ListofSubordinates = new List<IEmployee>();
        private readonly List<IEmployee> _employees = new List<IEmployee>();
        public List<IEmployee> Subordinates => _employees??(new List<IEmployee>());

        public void AddSubordinates(IEmployee employee)
        {
            _employees.Add(employee);
        }
        public void PerformanceSummary()
        {
            Console.WriteLine("\nPerformance summary of employee: " +
                              $"{Name} is {Rating} out of 5");
        }
    }
}
