﻿using System;

namespace ChainOfResponsibilitiesMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            // Setup chain of responsibilities
            Approver rony = new Director();
            Approver bobby = new VicePresident();
            Approver ricky = new President();

            rony.SetSuccessor(bobby);
            bobby.SetSuccessor(ricky);

            //Generate and process the requests
            Purshase p = new Purshase{Number = 8884,Amount = 350.00,Purpose = "Assets"};
            rony.ProcessRequest(p);

            p = new Purshase { Number = 5675, Amount = 33390.10, Purpose = "Project Poison" };
            rony.ProcessRequest(p);

            p = new Purshase { Number = 5676, Amount = 144400.00, Purpose = "Project BBD" };
            rony.ProcessRequest(p);

            //Wait for the user

        }
    }

    // The Handler abstract class
    public abstract class Approver
    {
        protected Approver _successor;

        public void SetSuccessor(Approver successor)
        {
            _successor = successor;
        }

        public abstract void ProcessRequest(Purshase purshase);
    }
    //Class holding request
    public class Purshase
    {
        public double Amount { get; set; }
        public int Number { get; set; }
        public string Purpose { get; set; }
    }
    // ConcreteHandler Class
    public class Director : Approver
    {
        public override void ProcessRequest(Purshase purshase)
        {
            if (purshase.Amount <10000.0)
            {
                Console.WriteLine($"{this.GetType().Name} approved request# {purshase.Number}");
            }else
            {
                _successor?.ProcessRequest(purshase);
            }
        }
    }
    // ConcreteHandler Class
    public class VicePresident : Approver
    {
        public override void ProcessRequest(Purshase purshase)
        {
            if (purshase.Amount<25000.0)
            {
                Console.WriteLine($"{this.GetType().Name} approved request# {purshase.Number}");
            }
            else
            {
                _successor?.ProcessRequest(purshase);
            }
        }
    }
    //ConcreteHandler Class
    public class President : Approver
    {
        public override void ProcessRequest(Purshase purshase)
        {
            if (purshase.Amount<100000.0)
            {
                Console.WriteLine($"{this.GetType().Name} approved request# {purshase.Number}");
            }
            else
            {
                Console.WriteLine($"Request# {purshase.Number} requires executive meeting!");
            }
        }
    }
}
