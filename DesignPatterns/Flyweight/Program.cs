﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            ShapeObjectFactory sof = new ShapeObjectFactory();
            IShape shape = sof.GetShape("Triangle");
            shape.Print();

            shape = sof.GetShape("Triangle");
            shape.Print();
            shape = sof.GetShape("Triangle");
            shape.Print();

            shape = sof.GetShape("Square");
            shape.Print();
            shape = sof.GetShape("Square");
            shape.Print();
            shape = sof.GetShape("Square");
            shape.Print();

            Console.WriteLine($"Number of objects created {sof.TotalObjectsCreated}");
        }
    }

    //Flyweight Interface
    public interface IShape
    {
        void Print();
    }
    //ConcreteFlyweight Class
    public class Triangle:IShape
    {
        public void Print()
        {
            Console.WriteLine("Printing Triangle");
        }
    }
    //ConcreteFlyweight Class
    public class Square : IShape
    {
        public void Print()
        {
            Console.WriteLine("Printing Square");
        }
    }
    //FlyweightFactory class
    public class ShapeObjectFactory
    {
        Dictionary<string,IShape> _shapes = new Dictionary<string, IShape>();
        public int TotalObjectsCreated => _shapes.Count;

        public IShape GetShape(string shapeName)
        {
            IShape shape = null;
            if (_shapes.ContainsKey(shapeName))
            {
                shape = _shapes[shapeName];
            }
            else
            {
                switch (shapeName)
                {
                    case "Triangle":
                        shape = new Triangle();
                        _shapes.Add(shapeName, shape);

                        break;
                    case "Square":
                        shape = new Square();
                        _shapes.Add(shapeName, shape);

                        break;
                    default:
                        throw new Exception("the factory cannot create the object");
                }
            }
            return shape;
        }
    }
}
